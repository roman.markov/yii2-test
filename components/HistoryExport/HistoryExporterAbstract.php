<?php

namespace app\components\HistoryExport;

use app\components\HistoryExport\DTOs\DataForRenderDTO;
use app\components\HistoryExport\interfaces\HistoryExporterInterface;
use app\models\History;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\db\QueryInterface;

abstract class HistoryExporterAbstract implements HistoryExporterInterface
{
    abstract public function getDataForRender(array $params): DataForRenderDTO;

    protected function getQuery(array $params = []): QueryInterface
    {
        $query = History::find();
        $query->addSelect('history.*');
        $query->with([
            'customer',
            'user',
            'sms',
            'task',
            'call',
            'fax',
        ]);
        return $query;
    }

    protected function getDataProvider(QueryInterface $query): DataProviderInterface
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'defaultOrder' => [
                'ins_ts' => SORT_DESC,
                'id' => SORT_DESC
            ],
        ]);

        return $dataProvider;
    }
}
