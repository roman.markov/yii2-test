<?php

namespace app\components\HistoryExport\interfaces;

use app\components\HistoryExport\DTOs\DataForRenderDTO;

interface HistoryExporterInterface
{
    function getDataForRender(array $params): DataForRenderDTO;
}
