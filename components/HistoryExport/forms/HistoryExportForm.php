<?php

namespace app\components\HistoryExport\forms;

use yii\base\Model;

class HistoryExportForm extends Model
{
    public $exportType;

    public function rules(): array
    {
        return [
            [['exportType'], 'required'],
            [['exportType'], 'string'],
        ];
    }
}
