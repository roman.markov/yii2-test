<?php

namespace app\components\HistoryExport\services;

use app\components\HistoryExport\DTOs\DataForRenderDTO;
use app\components\HistoryExport\HistoryExporterFactory;
use Yii;

class HistoryExportService
{
    /**
     * @var HistoryExporterFactory
     */
    protected $historyExporterFactory;

    public function __construct()
    {
        $this->historyExporterFactory = Yii::$container->get(HistoryExporterFactory::class);
    }

    /**
     * @param string $exportType
     * @param array $params
     * @return DataForRenderDTO
     * @throws \app\components\HistoryExport\exceptions\HistoryExporterUnsupportedTypeException
     */
    public function getDataForRender(string $exportType, array $params): DataForRenderDTO
    {
        $historyExporter = $this->historyExporterFactory->getExporterByType($exportType);
        return $historyExporter->getDataForRender($params);
    }
}

Yii::$container->set(HistoryExportService::class, []);
