<?php

namespace app\components\HistoryExport;

use app\components\HistoryExport\DTOs\DataForRenderDTO;
use app\widgets\HistoryList\helpers\HistoryListItemHelper;

class HistoryExporterCSV extends HistoryExporterAbstract
{
    public function getDataForRender(array $params): DataForRenderDTO
    {
        $result = new DataForRenderDTO();
        $query = $this->getQuery($params);
        $dataProvider = $this->getDataProvider($query);
        $result->view = 'exports/csv';
        $result->dataProvider = $dataProvider;
        $result->payload = [
            'historyListItemHelper' => new HistoryListItemHelper(),
        ];
        return $result;
    }
}
