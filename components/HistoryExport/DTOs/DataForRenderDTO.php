<?php

namespace app\components\HistoryExport\DTOs;

use yii\data\ActiveDataProvider;

class DataForRenderDTO
{
    /**
     * @var string
     */
    public $view;

    /**
     * @var array
     */
    public $payload = [];

    /**
     * @var ActiveDataProvider
     */
    public $dataProvider;
}
