<?php

namespace app\components\HistoryExport;
use app\components\HistoryExport\exceptions\HistoryExporterUnsupportedTypeException;
use app\components\HistoryExport\interfaces\HistoryExporterInterface;
use app\components\HistoryExport\types\ExportType;

class HistoryExporterFactory
{
    /**
     * @param string $exportType
     * @return HistoryExporterInterface
     * @throws HistoryExporterUnsupportedTypeException
     */
    public function getExporterByType(string $exportType): HistoryExporterInterface
    {
        if ($exportType === ExportType::CSV) {
            return new HistoryExporterCSV();
        }
        throw new HistoryExporterUnsupportedTypeException();
    }
}
