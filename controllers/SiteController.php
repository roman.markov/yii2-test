<?php

namespace app\controllers;

use app\components\HistoryExport\exceptions\HistoryExporterUnsupportedTypeException;
use app\components\HistoryExport\forms\HistoryExportForm;
use app\components\HistoryExport\services\HistoryExportService;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{
    /**
     * @var HistoryExportService
     */
    protected $historyExportService;

    public function __construct($id, $module, $config = [])
    {
        $this->historyExportService = Yii::$container->get(HistoryExportService::class);
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }


    /**
     * @param string $exportType
     * @return string|Response
     */
    public function actionExport(string $exportType)
    {
        $historyExportForm = new HistoryExportForm();
        $isValid = $historyExportForm->load(
            ['HistoryExportForm' => Yii::$app->request->get()]
        )
            && $historyExportForm->validate();

        if (!$isValid) {
            Yii::$app->session->setFlash(
                'error',
                implode(', ', $historyExportForm->getErrorSummary(true))
            );
            return $this->redirect(['site/index']);
        }

        $queryParams = Yii::$app->request->queryParams;
        try {
            $dataForRender = $this->historyExportService->getDataForRender($exportType, $queryParams);
        } catch (HistoryExporterUnsupportedTypeException $exception) {
            Yii::$app->session->setFlash(
                'error',
                $exportType . ' is not supported'
            );
            return $this->redirect(['site/index']);
        }

        return $this->render($dataForRender->view, [
            'dataProvider' => $dataForRender->dataProvider,
            'payload' => $dataForRender->payload,
        ]);
    }
}
