<?php

namespace app\widgets\HistoryList\helpers;

use app\models\History;
use app\widgets\HistoryList\HistoryListItems\HistoryListItemCompletedTask;
use app\widgets\HistoryList\HistoryListItems\HistoryListItemCreatedTask;
use app\widgets\HistoryList\HistoryListItems\HistoryListItemCustomerChangeQuality;
use app\widgets\HistoryList\HistoryListItems\HistoryListItemCustomerChangeType;
use app\widgets\HistoryList\HistoryListItems\HistoryListItemDefault;
use app\widgets\HistoryList\HistoryListItems\HistoryListItemIncomingCall;
use app\widgets\HistoryList\HistoryListItems\HistoryListItemIncomingFax;
use app\widgets\HistoryList\HistoryListItems\HistoryListItemIncomingSMS;
use app\widgets\HistoryList\HistoryListItems\HistoryListItemOutgoingCall;
use app\widgets\HistoryList\HistoryListItems\HistoryListItemOutgoingFax;
use app\widgets\HistoryList\HistoryListItems\HistoryListItemOutgoingSMS;

class HistoryListItemHelper
{
    public function getItemByEvent(string $event, History $model)
    {
        if ($event === History::EVENT_CREATED_TASK) {
            return new HistoryListItemCreatedTask($model);
        }
        if ($event === History::EVENT_COMPLETED_TASK) {
            return new HistoryListItemCompletedTask($model);
        }
        if ($event === History::EVENT_INCOMING_SMS) {
            return new HistoryListItemIncomingSMS($model);
        }
        if ($event === History::EVENT_OUTGOING_SMS) {
            return new HistoryListItemOutgoingSMS($model);
        }
        if ($event === History::EVENT_INCOMING_FAX) {
            return new HistoryListItemIncomingFax($model);
        }
        if ($event === History::EVENT_OUTGOING_FAX) {
            return new HistoryListItemOutgoingFax($model);
        }
        if ($event === History::EVENT_CUSTOMER_CHANGE_TYPE) {
            return new HistoryListItemCustomerChangeType($model);
        }
        if ($event === History::EVENT_CUSTOMER_CHANGE_QUALITY) {
            return new HistoryListItemCustomerChangeQuality($model);
        }
        if ($event === History::EVENT_INCOMING_CALL) {
            return new HistoryListItemIncomingCall($model);
        }
        if ($event === History::EVENT_OUTGOING_CALL) {
            return new HistoryListItemOutgoingCall($model);
        }
        return new HistoryListItemDefault($model);
    }
}
