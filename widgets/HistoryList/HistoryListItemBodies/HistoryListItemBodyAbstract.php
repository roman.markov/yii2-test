<?php

namespace app\widgets\HistoryList\HistoryListItemBodies;

use app\models\History;

abstract class HistoryListItemBodyAbstract implements HistoryListItemBodyInterface
{
    /**
     * @var History
     */
    protected $model;

    public function __construct(History $model)
    {
       $this->model = $model;
    }

    abstract function getBody(): string;
}
