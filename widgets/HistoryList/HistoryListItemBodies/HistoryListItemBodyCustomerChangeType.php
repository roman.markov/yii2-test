<?php

namespace app\widgets\HistoryList\HistoryListItemBodies;

use app\models\Customer;

class HistoryListItemBodyCustomerChangeType extends HistoryListItemBodyAbstract
{
    public function getBody(): string
    {
        return "{$this->model->eventText} " .
            (Customer::getTypeTextByType($this->model->getDetailOldValue('type')) ?? "not set") . ' to ' .
            (Customer::getTypeTextByType($this->model->getDetailNewValue('type')) ?? "not set");
    }
}
