<?php

namespace app\widgets\HistoryList\HistoryListItemBodies;

use app\models\Call;

class HistoryListItemBodyCall extends HistoryListItemBodyAbstract
{
    public function getBody(): string
    {
        /** @var Call $call */
        $call = $this->model->call;
        return ($call ? $call->totalStatusText . ($call->getTotalDisposition(false) ? " <span class='text-grey'>" . $call->getTotalDisposition(false) . "</span>" : "") : '<i>Deleted</i> ');

    }
}
