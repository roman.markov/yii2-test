<?php

namespace app\widgets\HistoryList\HistoryListItemBodies;

use app\models\Customer;

class HistoryListItemBodyCustomerChangeQuality extends HistoryListItemBodyAbstract
{
    public function getBody(): string
    {
        return "{$this->model->eventText} " .
            (Customer::getQualityTextByQuality($this->model->getDetailOldValue('quality')) ?? "not set") . ' to ' .
            (Customer::getQualityTextByQuality($this->model->getDetailNewValue('quality')) ?? "not set");
    }
}
