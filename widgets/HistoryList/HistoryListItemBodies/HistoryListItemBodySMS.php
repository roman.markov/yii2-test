<?php

namespace app\widgets\HistoryList\HistoryListItemBodies;

class HistoryListItemBodySMS extends HistoryListItemBodyAbstract
{
    public function getBody(): string
    {
        return $this->model->sms->message ? $this->model->sms->message : '';
    }
}
