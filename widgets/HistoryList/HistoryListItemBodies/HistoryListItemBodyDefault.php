<?php

namespace app\widgets\HistoryList\HistoryListItemBodies;

class HistoryListItemBodyDefault extends HistoryListItemBodyAbstract
{
    public function getBody(): string
    {
        return $this->model->eventText;
    }
}
