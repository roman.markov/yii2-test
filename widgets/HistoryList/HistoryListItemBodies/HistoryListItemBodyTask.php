<?php

namespace app\widgets\HistoryList\HistoryListItemBodies;

class HistoryListItemBodyTask extends HistoryListItemBodyAbstract
{
    public function getBody(): string
    {
        $task = $this->model->task;
        return "{$this->model->eventText}: " . ($task->title ?? '');
    }
}
