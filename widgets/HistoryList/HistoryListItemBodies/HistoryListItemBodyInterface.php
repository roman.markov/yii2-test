<?php

namespace app\widgets\HistoryList\HistoryListItemBodies;

interface HistoryListItemBodyInterface
{
    public function getBody(): string;
}
