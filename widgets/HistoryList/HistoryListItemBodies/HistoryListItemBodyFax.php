<?php

namespace app\widgets\HistoryList\HistoryListItemBodies;

use Yii;
use yii\helpers\Html;

class HistoryListItemBodyFax extends HistoryListItemBodyAbstract
{
    public function getBody(): string
    {
        $fax = $this->model->fax;
        return $this->model->eventText .
            ' - ' .
            (isset($fax->document) ? Html::a(
                Yii::t('app', 'view document'),
                $fax->document->getViewUrl(),
                [
                    'target' => '_blank',
                    'data-pjax' => 0
                ]
            ) : '');
    }
}
