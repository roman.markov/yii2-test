<?php

namespace app\widgets\HistoryList\HistoryListItems;

use app\widgets\HistoryList\HistoryListItemBodies\HistoryListItemBodyDefault;

class HistoryListItemDefault extends HistoryListItemAbstract
{
    public function getBody(): string
    {
        $historyListItemBody = new HistoryListItemBodyDefault($this->model);
        return $historyListItemBody->getBody();
    }

    public function getParamsForRender(): array
    {
        return [
            'user' => $this->model->user,
            'body' => $this->getBody(),
            'bodyDatetime' => $this->model->ins_ts,
            'iconClass' => 'fa-gear bg-purple-light'
        ];
    }
}
