<?php

namespace app\widgets\HistoryList\HistoryListItems;

use app\models\Customer;
use app\widgets\HistoryList\HistoryListItemBodies\HistoryListItemBodyCustomerChangeType;

class HistoryListItemCustomerChangeType extends HistoryListItemAbstract
{
    public function getBody(): string
    {
        $historyListItemBody = new HistoryListItemBodyCustomerChangeType($this->model);
        return $historyListItemBody->getBody();
    }

    public function getParamsForRender(): array
    {
        return [
            'model' => $this->model,
            'oldValue' => Customer::getTypeTextByType($this->model->getDetailOldValue('type')),
            'newValue' => Customer::getTypeTextByType($this->model->getDetailNewValue('type'))
        ];
    }

    public function getView(): string
    {
        return '_item_statuses_change';
    }
}
