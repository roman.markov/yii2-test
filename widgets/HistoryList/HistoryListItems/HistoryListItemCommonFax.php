<?php

namespace app\widgets\HistoryList\HistoryListItems;

use app\widgets\HistoryList\HistoryListItemBodies\HistoryListItemBodyFax;
use Yii;
use yii\helpers\Html;

class HistoryListItemCommonFax extends HistoryListItemAbstract
{
    public function getBody(): string
    {
        $historyListItemBody = new HistoryListItemBodyFax($this->model);
        return $historyListItemBody->getBody();
    }

    public function getParamsForRender(): array
    {
        $fax = $this->model->fax;
        return [
            'user' => $this->model->user,
            'body' => $this->getBody(),
            'footer' => Yii::t('app', '{type} was sent to {group}', [
                'type' => $fax ? $fax->getTypeText() : 'Fax',
                'group' => isset($fax->creditorGroup) ? Html::a($fax->creditorGroup->name, ['creditors/groups'], ['data-pjax' => 0]) : ''
            ]),
            'footerDatetime' => $this->model->ins_ts,
            'iconClass' => 'fa-fax bg-green'
        ];
    }
}
