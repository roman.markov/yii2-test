<?php

namespace app\widgets\HistoryList\HistoryListItems;

use app\models\Customer;
use app\widgets\HistoryList\HistoryListItemBodies\HistoryListItemBodyCustomerChangeQuality;

class HistoryListItemCustomerChangeQuality extends HistoryListItemAbstract
{
    public function getBody(): string
    {
        $historyListItemBody = new HistoryListItemBodyCustomerChangeQuality($this->model);
        return $historyListItemBody->getBody();
    }

    public function getParamsForRender(): array
    {
        return [
            'model' => $this->model,
            'oldValue' => Customer::getTypeTextByType($this->model->getDetailOldValue('quality')),
            'newValue' => Customer::getTypeTextByType($this->model->getDetailNewValue('quality'))
        ];
    }

    public function getView(): string
    {
        return '_item_statuses_change';
    }
}
