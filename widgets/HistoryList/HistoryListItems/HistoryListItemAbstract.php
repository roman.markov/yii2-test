<?php

namespace app\widgets\HistoryList\HistoryListItems;

use app\models\History;

abstract class HistoryListItemAbstract
{
    /**
     * @var History
     */
    protected $model;

    public function __construct(History $model)
    {
        $this->model = $model;
    }

    public function getView(): string
    {
        return '_item_common';
    }

    abstract public function getParamsForRender(): array;
}
