<?php

namespace app\widgets\HistoryList\HistoryListItems;

use app\widgets\HistoryList\HistoryListItemBodies\HistoryListItemBodyTask;

class HistoryListItemCommonTask extends HistoryListItemAbstract
{
    public function getBody(): string
    {
        $historyListItemBody = new HistoryListItemBodyTask($this->model);
        return $historyListItemBody->getBody();
    }

    public function getParamsForRender(): array
    {
        $task = $this->model->task;
        return [
            'user' => $this->model->user,
            'body' => $this->getBody(),
            'iconClass' => 'fa-check-square bg-yellow',
            'footerDatetime' => $this->model->ins_ts,
            'footer' => isset($task->customerCreditor->name) ? "Creditor: " . $task->customerCreditor->name : ''
        ];
    }
}
