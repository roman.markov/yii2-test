<?php

namespace app\widgets\HistoryList\HistoryListItems;

use app\models\Call;
use app\widgets\HistoryList\HistoryListItemBodies\HistoryListItemBodyCall;

class HistoryListItemCommonCall extends HistoryListItemAbstract
{
    public function getBody(): string
    {
        $historyListItemBody = new HistoryListItemBodyCall($this->model);
        return $historyListItemBody->getBody();
    }

    public function getParamsForRender(): array
    {
        /** @var Call $call */
        $call = $this->model->call;
        $answered = $call && $call->status == Call::STATUS_ANSWERED;
        return [
            'user' => $this->model->user,
            'content' => $call->comment ?? '',
            'body' => $this->getBody(),
            'footerDatetime' => $this->model->ins_ts,
            'footer' => isset($call->applicant) ? "Called <span>{$call->applicant->name}</span>" : null,
            'iconClass' => $answered ? 'md-phone bg-green' : 'md-phone-missed bg-red',
            'iconIncome' => $answered && $call->direction == Call::DIRECTION_INCOMING
        ];
    }
}
