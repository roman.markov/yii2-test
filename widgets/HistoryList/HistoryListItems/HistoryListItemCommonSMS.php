<?php

namespace app\widgets\HistoryList\HistoryListItems;

use app\models\Sms;
use app\widgets\HistoryList\HistoryListItemBodies\HistoryListItemBodySMS;
use Yii;

class HistoryListItemCommonSMS extends HistoryListItemAbstract
{
    public function getBody(): string
    {
        $historyListItemBody = new HistoryListItemBodySMS($this->model);
        return $historyListItemBody->getBody();
    }

    public function getParamsForRender(): array
    {
        return [
            'user' => $this->model->user,
            'body' => $this->getBody(),
            'footer' => $this->model->sms->direction == Sms::DIRECTION_INCOMING ?
                Yii::t('app', 'Incoming message from {number}', [
                    'number' => $this->model->sms->phone_from ?? ''
                ]) : Yii::t('app', 'Sent message to {number}', [
                    'number' => $this->model->sms->phone_to ?? ''
                ]),
            'iconIncome' => $this->model->sms->direction == Sms::DIRECTION_INCOMING,
            'footerDatetime' => $this->model->ins_ts,
            'iconClass' => 'icon-sms bg-dark-blue'
        ];
    }
}
