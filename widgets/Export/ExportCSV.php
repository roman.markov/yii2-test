<?php

namespace app\widgets\Export;

use kartik\export\ExportMenu;
use Yii;

class ExportCSV extends ExportMenu
{
    public $exportType = self::FORMAT_CSV;

    public function init()
    {
        $this->setupExportParams();
        parent::init();
    }

    protected function setupExportParams()
    {
        if (empty($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }

        if (empty($this->exportRequestParam)) {
            $this->exportRequestParam = 'exportFull_' . $this->options['id'];
        }

        $params = Yii::$app->request->getBodyParams();

        $params[Yii::$app->request->methodParam] = 'POST';
        $params[$this->exportRequestParam] = true;
        $params[$this->exportTypeParam] = $this->exportType;
        $params[$this->colSelFlagParam] = false;

        Yii::$app->request->setBodyParams($params);
    }
}
